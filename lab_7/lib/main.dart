import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Form Pendonor",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  String? _nama;
  String? _nik;
  String? _tl;
  String? _goldar;
  String? _rhesus;
  String? _provinsi;
  String? _kota;
  String? _hp;
  String? _linkswabpos;
  String? _linkswabneg;
  String? _ts;
  String? _tm;
  // final namaCon = TextEditingController();
  // final nikCon = TextEditingController();
  // final tlCon = TextEditingController();
  // final goldar = TextEditingController();
  // final rhesus = TextEditingController();
  // final provinsi = TextEditingController();
  // final kota = TextEditingController();
  // final hp = TextEditingController();
  // final linkswabpos = TextEditingController();
  // final linkswabneg = TextEditingController();
  // final ts = TextEditingController();
  // final tm = TextEditingController();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  // @override
  // void dispose() {
  //   namaCon.dispose();
  //   nikCon.dispose();
  //   tlCon.dispose();
  //   goldar.dispose();
  //   rhesus.dispose();
  //   provinsi.dispose();
  //   kota.dispose();
  //   hp.dispose();
  //   linkswabneg.dispose();
  //   linkswabpos.dispose();
  //   ts.dispose();
  //   tm.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Pendonor"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: Susilo Bambang",
                      labelText: "Nama Lengkap",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: namaCon,
                    onSaved: (String? value) {
                      _nama = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "NIK",
                      icon: Icon(Icons.security),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'NIK tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: nikCon,
                    onSaved: (String? value) {
                      _nik = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Format : DD/MM/YYYY",
                      labelText: "Tanggal Lahir",
                      icon: Icon(Icons.calendar_today),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Tanggal lahir tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: tlCon,
                    onSaved: (String? value) {
                      _tl = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "O/A/B/AB",
                      labelText: "Golongan Darah",
                      icon: Icon(Icons.bloodtype),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Golongan Darah tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: goldar,
                    onSaved: (String? value) {
                      _goldar = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "+/-",
                      labelText: "Rhesus",
                      icon: Icon(Icons.bloodtype),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Rhesus tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: rhesus,
                    onSaved: (String? value) {
                      _rhesus = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      // hintText: "Format : DD/MM/YYYY",
                      labelText: "Provinsi",
                      icon: Icon(Icons.location_city),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Provinsi tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: provinsi,
                    onSaved: (String? value) {
                      _provinsi = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      // hintText: "Format : DD/MM/YYYY",
                      labelText: "Kota/Kab",
                      icon: Icon(Icons.location_city),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Kota/Kabupaten tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: kota,
                    onSaved: (String? value) {
                      _kota = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "ex. 08xxxxx",
                      labelText: "No. HP",
                      icon: Icon(Icons.phone),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'No. HP tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: hp,
                    onSaved: (String? value) {
                      _hp = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Dalam bentuk link menuju file",
                      labelText: "Bukti swab positif",
                      icon: Icon(Icons.link),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Bukti swab positif tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: linkswabpos,
                    onSaved: (String? value) {
                      _linkswabpos = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Dalam bentuk link menuju file",
                      labelText: "Bukti Swab Negatif",
                      icon: Icon(Icons.link),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Bukti swab negatif tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: linkswabneg,
                    onSaved: (String? value) {
                      _linkswabneg = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Format : DD/MM/YYYY",
                      labelText: "Tanggal Sembuh",
                      icon: Icon(Icons.calendar_today),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Tanggal sembuh tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: ts,
                    onSaved: (String? value) {
                      _ts = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Format : DD/MM/YYYY",
                      labelText: "Tanggal Terakhir Mendonor",
                      icon: Icon(Icons.calendar_today),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value != null && value.isEmpty) {
                        return 'Tanggal terakhir mendonor tidak boleh kosong';
                      }
                      return null;
                    },
                    // controller: tm,
                    onSaved: (String? value) {
                      _tm = value;
                    },
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: TextFormField(
                //     obscureText: true,
                //     decoration: new InputDecoration(
                //       labelText: "Password",
                //       icon: Icon(Icons.security),
                //       border: OutlineInputBorder(
                //           borderRadius: new BorderRadius.circular(5.0)),
                //     ),
                //     validator: (value) {
                //       if (value != null && value.isEmpty) {
                //         return 'Password tidak boleh kosong';
                //       }
                //       return null;
                //     },
                //   ),
                // ),
                // CheckboxListTile(
                //   title: Text('Belajar Dasar Flutter'),
                //   subtitle: Text('Dart, widget, http'),
                //   value: nilaiCheckBox,
                //   activeColor: Colors.deepPurpleAccent,
                //   onChanged: (bool? value) {
                //     setState(() {
                //       nilaiCheckBox = value!;
                //     });
                //   },
                // ),
                // SwitchListTile(
                //   title: Text('Backend Programming'),
                //   subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                //   value: nilaiSwitch,
                //   activeTrackColor: Colors.pink[100],
                //   activeColor: Colors.red,
                //   onChanged: (value) {
                //     setState(() {
                //       nilaiSwitch = value;
                //     });
                //   },
                // ),
                // Slider(
                //   value: nilaiSlider,
                //   min: 0,
                //   max: 100,
                //   onChanged: (value) {
                //     setState(() {
                //       nilaiSlider = value;
                //     });
                //   },
                // ),
                ElevatedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                            content: Text('Data Pendonor berhasil disimpan')),
                      );
                      print("Data Pendonor:");
                      print("Nama : " + _nama!);
                      print("NIK : " + _nik!);
                      print("Tanggal Lahir : " + _tl!);
                      print("Golongan Darah : " + _goldar!);
                      print("Rhesus : " + _rhesus!);
                      print("Provinsi : " + _provinsi!);
                      print("Kota : " + _kota!);
                      print("No. HP : " + _hp!);
                      print("Bukti Swab Positif : " + _linkswabpos!);
                      print("Bukti Swab Negatif : " + _linkswabneg!);
                      print("Tanggal Sembuh : " + _ts!);
                      print("Tanggal Terakhir Mendonor : " + _tm!);
                      _formKey.currentState!.reset();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
