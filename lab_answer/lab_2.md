1. Apa perbedaan antara JSON dan XML?
JSON (Javascript Object Notation) berbentuk sebagai suatu object sementara XML (Extensible Markup Language) berbentuk sebagai markup language. Meskipun sama-sama untuk pertukaran data, JSON bekerja lebih cepat/efisien daripada XML. Selain itu, JSON juga terlihat lebih rapi dan sederhana dibanding XML

2. Apa perbedaan antara HTML dan XML?
Meskipun sama-sama berbentuk markup language, XML memiliki fungsi utama untuk pertukaran data sementara HTML hanya sebagai penyajian data. Di XML kita bisa membuat tag khusus sesuai nama data/ jenis data yang dibutuhkan.