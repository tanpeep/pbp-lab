from django.shortcuts import render
from lab_2.models import Note
from django.core.serializers import serialize
from django.http.response import HttpResponse, JsonResponse
import json

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes':notes}
    return render(request,'lab5_index.html',response)

def get_note(request,id):
    response = {}
    data = Note.objects.get(id=id)
    response = serialize("json", [data])
    # data = serializers.serialize('json',Note.objects.get(id = id))
    return HttpResponse(response,content_type = "application/json")