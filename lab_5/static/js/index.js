$(document).ready(function () {
    $.getJSON('/lab-2/json', function (data) {
        for (let i = 0; i < data.length; i++) {
            var note = data[i]["fields"];
            $("tbody").append(
                `<tr>
                <td>${note["to"]}</td>
                <td>${note["From"]}</td>
                <td>${note["title"]}</td>
                <td>${note["message"]}</td>
                <td><button>View</button><button>Edit</button><button>Delete</button></td>
                </tr>`
            );
        }
    });
});
