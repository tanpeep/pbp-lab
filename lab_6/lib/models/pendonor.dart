import 'package:flutter/foundation.dart';

enum GolonganDarah {
  A,
  B,
  O,
  AB,
}

enum Provinsi {
  Aceh,
  Jawa_Barat,
  DKI_Jakarta,
}

enum Kota {
  Banda_Aceh,
  Depok,
  Bandung,
  Jakarta_Pusat,
}

class Pendonor {
  final String id;
  final String nama;
  final String nik;
  final String tanggal_lahir;
  final String goldar;
  final String rhesus;
  final String provinsi;
  final String kota;
  final String nohp;
  final String tanggalSembuh;
  final String tanggalmendonor;

  const Pendonor(
      this.id,
      this.nama,
      this.nik,
      this.tanggal_lahir,
      this.goldar,
      this.rhesus,
      this.provinsi,
      this.kota,
      this.nohp,
      this.tanggalSembuh,
      this.tanggalmendonor);
}
