import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../dummy_data.dart';

class InfoScreen extends StatelessWidget {
  static const routeName = '/infoPendonor';
  const InfoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Informasi pendonor"),
      ),
      drawer: MainDrawer(),
      body: Card(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Nama : " + Dummy_Pendonor[0].nama,
              style: TextStyle(fontSize: 20)),
          Text('NIK : ' + Dummy_Pendonor[0].nik,
              style: TextStyle(fontSize: 20)),
          Text("Tanggal Lahir : " + Dummy_Pendonor[0].tanggal_lahir,
              style: TextStyle(fontSize: 20)),
          Text("Gol. Darah : " + Dummy_Pendonor[0].goldar,
              style: TextStyle(fontSize: 20)),
          Text("Rhesus : " + Dummy_Pendonor[0].rhesus,
              style: TextStyle(fontSize: 20)),
          Text("Provinsi : " + Dummy_Pendonor[0].provinsi,
              style: TextStyle(fontSize: 20)),
          Text("Kota : " + Dummy_Pendonor[0].kota,
              style: TextStyle(fontSize: 20)),
          Text("No. HP : " + Dummy_Pendonor[0].nohp,
              style: TextStyle(fontSize: 20)),
          Text("Tanggal Sembuh : " + Dummy_Pendonor[0].tanggalSembuh,
              style: TextStyle(fontSize: 20)),
          Text("Tgl. mendonor : " + Dummy_Pendonor[0].tanggalmendonor,
              style: TextStyle(fontSize: 20)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {},
                  child: const Text("Ubah Data"),
                  style: ElevatedButton.styleFrom(primary: Colors.grey)),
              ElevatedButton(
                  onPressed: () {},
                  child: const Text("Batal Mendonor"),
                  style: ElevatedButton.styleFrom(primary: Colors.red)),
              ElevatedButton(
                  onPressed: () {},
                  child: const Text("Sudah Mendonor"),
                  style: ElevatedButton.styleFrom(primary: Colors.green)),
            ],
          )
        ],
      )),
    );
  }
}
