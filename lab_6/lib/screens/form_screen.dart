import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../widgets/form_widget.dart';

class FormScreen extends StatelessWidget {
  static const routeName = '/daftarPendonor';
  const FormScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Daftar Mendonor"),
      ),
      drawer: MainDrawer(),
      body: MyCustomForm(),
    );
  }
}
