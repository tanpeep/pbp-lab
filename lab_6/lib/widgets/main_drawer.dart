import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';
import '../screens/form_screen.dart';
import '../screens/info_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(
      String title, IconData icon, void Function()? tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Donor!',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.restaurant, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          // buildListTile('Filter', Icons.settings, () {
          //   Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          // }),
          buildListTile('Daftar Pendonor', Icons.app_registration, () {
            Navigator.of(context).pushReplacementNamed(FormScreen.routeName);
          }),
          buildListTile("Info Pendonor", Icons.insert_drive_file_outlined, () {
            Navigator.of(context).pushReplacementNamed(InfoScreen.routeName);
          })
        ],
      ),
    );
  }
}
