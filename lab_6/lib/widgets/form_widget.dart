import 'package:flutter/material.dart';

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "This field is required";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Nama",
            ),
          ),
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "This field is required";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "NIK",
            ),
          ),
          InputDatePickerFormField(
            firstDate: DateTime.utc(1960, 1, 1),
            lastDate: DateTime.utc(2004, 12, 31),
            fieldLabelText: "Tanggal Lahir",
          ),
          DropdownButtonFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Golongan Darah",
            ),
            items: ["A", "B", "AB", "O"]
                .map<DropdownMenuItem<String>>(
                  (option) => DropdownMenuItem<String>(
                    value: option,
                    child: Text(option),
                  ),
                )
                .toList(),
            onChanged: (val) {},
          ),
          DropdownButtonFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Rhesus",
            ),
            // items: ["+","-"].map<DropdownMenuItem<String>>(value: option,child:Text(option))
            items: ["+", "-"]
                .map<DropdownMenuItem<String>>(
                  (option) => DropdownMenuItem<String>(
                    value: option,
                    child: Text(option),
                  ),
                )
                .toList(),
            onChanged: (val) {},
          ),
          DropdownButtonFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Provinsi",
            ),
            items: ["Aceh", "DKI Jakarta", "Jawa Barat"]
                .map<DropdownMenuItem<String>>(
                  (option) => DropdownMenuItem<String>(
                    value: option,
                    child: Text(option),
                  ),
                )
                .toList(),
            onChanged: (val) {},
          ),
          DropdownButtonFormField(
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "Kota",
            ),
            items: ["Banda Aceh", "Jakarta Pusat", "Depok", "Bandung"]
                .map<DropdownMenuItem<String>>(
                  (option) => DropdownMenuItem<String>(
                    value: option,
                    child: Text(option),
                  ),
                )
                .toList(),
            onChanged: (val) {},
          ),
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "This field is required";
              }
              return null;
            },
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: "No. HP",
            ),
          ),
          InputDatePickerFormField(
            firstDate: DateTime.utc(2020, 1, 1),
            lastDate: DateTime.now(),
            fieldLabelText: "Tanggal Sembuh",
          ),
          InputDatePickerFormField(
            firstDate: DateTime.utc(2020, 1, 1),
            lastDate: DateTime.now(),
            fieldLabelText: "Tanggal Terakhir Mendonor",
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text("Anda berhasil mendaftar")));
                }
              },
              child: const Text('Submit'),
            ),
          )
        ],
      ),
    );
  }
}
