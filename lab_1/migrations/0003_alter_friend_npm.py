# Generated by Django 3.2.7 on 2021-09-20 07:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0002_friend_dob'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='npm',
            field=models.CharField(max_length=20),
        ),
    ]
